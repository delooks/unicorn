# About

This is a basic setup for storyblok projects.

## Setup

Be sure to change apiKey, url and themeId in gulp/config.json

## Install npm and bower packages

```
npm install && bower install
```

## Start the server

```
gulp
```
