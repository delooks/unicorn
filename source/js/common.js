var $ = require('../../node_modules/uikit/vendor/jquery.js')

module.exports = {
  init: function() {
    $('[data-component]').each(function(i, el) {
      this[$(el).data('component')](el)
    }.bind(this))
  },
  loadScript(src) {
    return new Promise(function(resolve, reject) {
      var s
      s = document.createElement('script')
      s.src = src
      s.onload = resolve
      s.onerror = reject
      document.head.appendChild(s)
    })
  },
  loadStylesheet(url) {
    var s = document.createElement('link')
    s.type = 'text/css'
    s.href = url
    s.rel = 'stylesheet'
    var x = document.getElementsByTagName('head')[0]
    x.appendChild(s)
  }
}
