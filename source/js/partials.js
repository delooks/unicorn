var $ = require('../../node_modules/uikit/vendor/jquery.js')

module.exports = function() {
  var partials = [];

  $('[data-partial]').each(function() {
    var p = $(this).data('partial');
    if (partials.indexOf(p) === -1) {
      partials.push(p)
    }
  });

  $.ajax({
    url: site_url('partials/show'),
    type: 'GET',
    dataType: 'json',
    data: {paths: partials},
  })
  .done(function(data) {
    $('[data-partial]').each(function() {
      $(this).html(data[$(this).data('partial')]);
    });
  }.bind(this));
}
